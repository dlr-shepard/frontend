# shepard frontend

> This project has been moved to the [Mono Repository](https://gitlab.com/dlr-shepard/shepard/-/tree/main/frontend) and is now read-only.

The shepard frontend is based on [Vue.js](https://vuejs.org/) and [OpenAPI Generator](https://openapi-generator.tech/).
For more information about shepard, its usage and infrastructure, check out [the wiki](https://gitlab.com/dlr-shepard/documentation/-/wikis/home).

## Getting started as an administrator

Information about the deployment can be found [here](https://gitlab.com/dlr-shepard/deployment).

## Getting started as a contributor

Shepard is open for contributions.
Information on how to contribute can be found [here](CONTRIBUTING.md).

## Useful Commands

- Install node modules: `npm install`
- Update packages: `npm update`
- Find outdated packages: `npm outdated`
- Run dev server: `npm run dev`
- Run linter: `npm run lint`
- Fix linting issues: `npm run fix`
- Compile for production: `npm run build`
