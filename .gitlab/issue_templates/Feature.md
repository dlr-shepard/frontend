# Feature Request

<!--
Thanks for contributing to our project!
Before creating a feature request, please read this first:

- Use the template below
- Check if there are related issues
- Determine which repository the request should be reported in:
  https://gitlab.com/dlr-shepard/frontend/-/blob/main/CONTRIBUTING.md#how-can-i-contribute
- Use a clear and descriptive title for the issue to identify the suggestion
- For code snippets and logs please use Markdown code blocks:
  https://docs.gitlab.com/ee/user/markdown.html

-->

## Role-Feature-Reason Template

As a `role` I want `feature` so that `reason`.

## Category according to the [Kano Model](https://en.wikipedia.org/wiki/Kano_model)

<!-- Please check only 1 box -->

- [ ] must-be <!-- these are the requirements that the customers expect and are taken for granted -->
- [ ] performance <!-- these attributes result in satisfaction when fulfilled and dissatisfaction when not fulfilled -->
- [ ] attractive <!-- these attributes provide satisfaction when achieved fully, but do not cause dissatisfaction when not fulfilled -->

## Describe your new feature

<!--
Describe the new feature with as many details as possible
-->

### Current behavior

<!--
What is the current behavior?
-->

### New behavior

<!--
What would you expect the new behavior should look like?
-->

### Improvement

<!--
What is the benefit and why is it interesting for all users?
-->

### Screenshots and Mockups

<!--
paste in screenshots or mockups of your new feature
-->

## Related Issues

- #<issue number>

/label ~"issue::feature"
