# Bug Report

<!--
Thanks for contributing to our project!
Before creating a bug report, please read this first:

- Use the template below
- Check if there are related issues
- Determine which repository the request should be reported in:
  https://gitlab.com/dlr-shepard/frontend/-/blob/main/CONTRIBUTING.md#how-can-i-contribute
- Use a clear and descriptive title for the issue to identify the bug report
- If you encounter a security issue, please mark this bug report as confidential
- For code snippets and logs please use Markdown code blocks:
  https://docs.gitlab.com/ee/user/markdown.html

-->

## Environment

<!-- You can find out the exact version on the "About" page -->

- Backend Version:
- Frontend Version:
- Webbrowser and version:
- Operating system:

## Summary

<!--
Summarize the bug you encountered briefly and provide as much detail as possible:

- Has the issue occurred recently (e.g., after updating to a new version of shepard) or has it always been a problem?
- Can you reliably reproduce the issue? If not, please indicate how often the problem occurs and under what conditions it usually occurs.
- If the issue was not triggered by a specific action, describe what you did before the problem occurred and provide further information.

-->

## Steps to reproduce

<!--
Check if you can reproduce the issue in the latest version of the shepard frontend.
Describe the exact steps which reproduce the problem in as many details as possible.
-->

1.
2.
3.

## What is the current bug behavior?

<!--
What actually happens
-->

## What is the expected correct behavior?

<!--
What you should see instead
-->

## Relevant logs and/or screenshots

<!--
Paste all relevant logs from your web browser's console/inspector, as well as screenshots and animated GIFs here
Recommended tool for creating GIFs: https://www.screentogif.com/
-->

```txt
Paste logs here
```

## Possible fixes

<!--
If you can, link to the line of code that might be responsible for the problem
-->

## Related Issues

- #<issue number>

/label ~"issue::bug"
